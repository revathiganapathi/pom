package Steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	
	@Given("Open the Browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	}

	@Given("Max the Browser")
	public void maxTheBrowser() {
	    driver.manage().window().maximize();
	}

	@Given("Set the timeout")
	public void setTheTimeout() {
	    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS );
	}

	@Given("Launch the URL")
	public void launchTheURL() {
		
		driver.get("http://leaftaps.com/opentaps/control/main");    
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String username) {
		
		driver.findElementById("username").sendKeys(username);
	    
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String password) {
		
		driver.findElementById("password").sendKeys(password);
	    
	}

	@Given("Click on the login button")
	public void clickOnTheLoginButton() {
		
		driver.findElementByClassName("decorativeSubmit").click();
	    
	}

	@Given("Click on crmsfa")
	public void clickOnCrmsfa() {
		
		driver.findElementByLinkText("CRM/SFA").click(); 
	}

	@Given("Click on Leads")
	public void clickOnLeads() {
		driver.findElementByLinkText("Leads").click();
	    
	}

	@Given("Click on Createlead")
	public void clickOnCreatelead() {
		driver.findElementByLinkText("Create Lead").click();
	    
	}

	@Given("Enter the Companyname as (.*)")
	public void enterTheCompanyname(String cn) {
		
		driver.findElementById("createLeadForm_companyName").sendKeys(cn);
	    
	}

	@Given("Enter the Lastname as (.*)")
	public void enterTheLastname(String ln) {
	    
		driver.findElementById("createLeadForm_firstName").sendKeys(ln);
	}

	@Given("Enter the Firstname as (.*)")
	public void enterTheFirstname(String fn) {
		
		driver.findElementById("createLeadForm_lastName").sendKeys(fn);
	    
	}

	@When("Click the Createlead button")
	public void clickTheCreateleadButton() {
		
		driver.findElementByClassName("smallSubmit").click();
	    
	}

	@Then("Verify Createlead")
	public void verifyCreatelead() {
		
		
	    
	}

	
	
	
	
	

}
