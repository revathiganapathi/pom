Feature: Createlead

Background:
Given Open the Browser
And Max the Browser
And Set the timeout
And Launch the URL

Scenario Outline: Positive Createlead
And Enter the Username as <username> 
And Enter the Password as <password>
And Click on the login button
And Click on crmsfa
And Click on Leads
And Click on Createlead
And Enter the Companyname as <companyname>
And Enter the Lastname as <lastname>
And Enter the Firstname as <firstname>
When Click the Createlead button
Then Verify Createlead

Examples:
|username|password|companyname|lastname|firstname|
|DemoSalesManager|crmsfa|CTS|Ganapathi|Revathi|
|DemoCSR|crmsfa|IBM|Sri|Gayathri|

