package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateleadPage extends ProjectMethods{
	
	public CreateleadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	WebElement companyName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName")
	WebElement firstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName")
	WebElement lastName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit")
	WebElement eleCreatelead;
	
	public CreateleadPage companyName(String data) {
		type(companyName, data);
		return this;
	}
	public CreateleadPage firstName(String data) {
		type(firstName, data);
		return this;
	}
	public CreateleadPage lastName(String data) {
		type(lastName, data);
		return this;
	}
	public ViewLeadPage createLead() {
		
		click(eleCreatelead);
		return new ViewLeadPage();
	}
		

}
