package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Revathi";
		category = "smoke";
		dataSheetName = "TC002_CreateLead";
		testNodes = "Leads";
	}
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password,String companyname,String lastname,String firstname) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreate()
		.companyName(companyname)
		.lastName(lastname)
		.firstName(firstname)
		.createLead();
	
	}
	
	
}
